﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace csSQLdatabase
{
    public partial class FormDataSetTurma : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        Form main;

        public FormDataSetTurma(Form form)
        {
            InitializeComponent();
            main = form;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            main.Show();
        }

        private void panel4_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void turmaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.turmaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        private void FormDataSetTurma_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.ALuno_Turma' table. You can move, or remove it, as needed.
            this.aLuno_TurmaTableAdapter.Fill(this.bDlocalDataSet.ALuno_Turma);
            // TODO: This line of code loads data into the 'bDlocalDataSet.Turma' table. You can move, or remove it, as needed.
            this.turmaTableAdapter.Fill(this.bDlocalDataSet.Turma);

        }

        private void aLuno_TurmaDataGridView_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
