﻿namespace csSQLdatabase
{
    partial class FormDataSetTurma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormDataSetTurma));
            System.Windows.Forms.Label nTurmaLabel;
            System.Windows.Forms.Label cicloLetivonCicloLabel;
            System.Windows.Forms.Label anoLetivoLabel;
            System.Windows.Forms.Label descriLabel;
            System.Windows.Forms.Label aLunonProcLabel;
            System.Windows.Forms.Label turmanTurmaLabel;
            System.Windows.Forms.Label dataMatriculaLabel;
            this.button1 = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.bDlocalDataSet = new csSQLdatabase.BDlocalDataSet();
            this.turmaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.turmaTableAdapter = new csSQLdatabase.BDlocalDataSetTableAdapters.TurmaTableAdapter();
            this.tableAdapterManager = new csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager();
            this.turmaBindingNavigator = new System.Windows.Forms.BindingNavigator(this.components);
            this.bindingNavigatorMoveFirstItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMovePreviousItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorPositionItem = new System.Windows.Forms.ToolStripTextBox();
            this.bindingNavigatorCountItem = new System.Windows.Forms.ToolStripLabel();
            this.bindingNavigatorSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorMoveNextItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorMoveLastItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.bindingNavigatorAddNewItem = new System.Windows.Forms.ToolStripButton();
            this.bindingNavigatorDeleteItem = new System.Windows.Forms.ToolStripButton();
            this.turmaBindingNavigatorSaveItem = new System.Windows.Forms.ToolStripButton();
            this.nTurmaTextBox = new System.Windows.Forms.TextBox();
            this.cicloLetivonCicloTextBox = new System.Windows.Forms.TextBox();
            this.anoLetivoTextBox = new System.Windows.Forms.TextBox();
            this.descriTextBox = new System.Windows.Forms.TextBox();
            this.aLuno_TurmaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.aLuno_TurmaTableAdapter = new csSQLdatabase.BDlocalDataSetTableAdapters.ALuno_TurmaTableAdapter();
            this.turmaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aLuno_TurmaDataGridView = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aLunonProcTextBox = new System.Windows.Forms.TextBox();
            this.turmanTurmaTextBox = new System.Windows.Forms.TextBox();
            this.dataMatriculaDateTimePicker = new System.Windows.Forms.DateTimePicker();
            nTurmaLabel = new System.Windows.Forms.Label();
            cicloLetivonCicloLabel = new System.Windows.Forms.Label();
            anoLetivoLabel = new System.Windows.Forms.Label();
            descriLabel = new System.Windows.Forms.Label();
            aLunonProcLabel = new System.Windows.Forms.Label();
            turmanTurmaLabel = new System.Windows.Forms.Label();
            dataMatriculaLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingNavigator)).BeginInit();
            this.turmaBindingNavigator.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aLuno_TurmaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.aLuno_TurmaDataGridView)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.IndianRed;
            this.button1.Cursor = System.Windows.Forms.Cursors.Default;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Location = new System.Drawing.Point(488, 12);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Voltar";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.CornflowerBlue;
            this.panel4.Location = new System.Drawing.Point(12, 12);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(470, 23);
            this.panel4.TabIndex = 10;
            this.panel4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel4_MouseDown);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.LimeGreen;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Location = new System.Drawing.Point(569, 12);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Salvar";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // bDlocalDataSet
            // 
            this.bDlocalDataSet.DataSetName = "BDlocalDataSet";
            this.bDlocalDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // turmaBindingSource
            // 
            this.turmaBindingSource.DataMember = "Turma";
            this.turmaBindingSource.DataSource = this.bDlocalDataSet;
            // 
            // turmaTableAdapter
            // 
            this.turmaTableAdapter.ClearBeforeFill = true;
            // 
            // tableAdapterManager
            // 
            this.tableAdapterManager.ALuno_TurmaTableAdapter = this.aLuno_TurmaTableAdapter;
            this.tableAdapterManager.ALunoTableAdapter = null;
            this.tableAdapterManager.BackupDataSetBeforeUpdate = false;
            this.tableAdapterManager.CicloLetivoTableAdapter = null;
            this.tableAdapterManager.TurmaTableAdapter = this.turmaTableAdapter;
            this.tableAdapterManager.UpdateOrder = csSQLdatabase.BDlocalDataSetTableAdapters.TableAdapterManager.UpdateOrderOption.InsertUpdateDelete;
            // 
            // turmaBindingNavigator
            // 
            this.turmaBindingNavigator.AddNewItem = this.bindingNavigatorAddNewItem;
            this.turmaBindingNavigator.BindingSource = this.turmaBindingSource;
            this.turmaBindingNavigator.CountItem = this.bindingNavigatorCountItem;
            this.turmaBindingNavigator.DeleteItem = this.bindingNavigatorDeleteItem;
            this.turmaBindingNavigator.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.turmaBindingNavigator.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.bindingNavigatorMoveFirstItem,
            this.bindingNavigatorMovePreviousItem,
            this.bindingNavigatorSeparator,
            this.bindingNavigatorPositionItem,
            this.bindingNavigatorCountItem,
            this.bindingNavigatorSeparator1,
            this.bindingNavigatorMoveNextItem,
            this.bindingNavigatorMoveLastItem,
            this.bindingNavigatorSeparator2,
            this.bindingNavigatorAddNewItem,
            this.bindingNavigatorDeleteItem,
            this.turmaBindingNavigatorSaveItem});
            this.turmaBindingNavigator.Location = new System.Drawing.Point(0, 376);
            this.turmaBindingNavigator.MoveFirstItem = this.bindingNavigatorMoveFirstItem;
            this.turmaBindingNavigator.MoveLastItem = this.bindingNavigatorMoveLastItem;
            this.turmaBindingNavigator.MoveNextItem = this.bindingNavigatorMoveNextItem;
            this.turmaBindingNavigator.MovePreviousItem = this.bindingNavigatorMovePreviousItem;
            this.turmaBindingNavigator.Name = "turmaBindingNavigator";
            this.turmaBindingNavigator.PositionItem = this.bindingNavigatorPositionItem;
            this.turmaBindingNavigator.Size = new System.Drawing.Size(658, 25);
            this.turmaBindingNavigator.TabIndex = 12;
            this.turmaBindingNavigator.Text = "bindingNavigator1";
            // 
            // bindingNavigatorMoveFirstItem
            // 
            this.bindingNavigatorMoveFirstItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveFirstItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveFirstItem.Image")));
            this.bindingNavigatorMoveFirstItem.Name = "bindingNavigatorMoveFirstItem";
            this.bindingNavigatorMoveFirstItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveFirstItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMoveFirstItem.Text = "Move first";
            // 
            // bindingNavigatorMovePreviousItem
            // 
            this.bindingNavigatorMovePreviousItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMovePreviousItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMovePreviousItem.Image")));
            this.bindingNavigatorMovePreviousItem.Name = "bindingNavigatorMovePreviousItem";
            this.bindingNavigatorMovePreviousItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMovePreviousItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorMovePreviousItem.Text = "Move previous";
            // 
            // bindingNavigatorSeparator
            // 
            this.bindingNavigatorSeparator.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator.Size = new System.Drawing.Size(6, 25);
            // 
            // bindingNavigatorPositionItem
            // 
            this.bindingNavigatorPositionItem.AccessibleName = "Position";
            this.bindingNavigatorPositionItem.AutoSize = false;
            this.bindingNavigatorPositionItem.Name = "bindingNavigatorPositionItem";
            this.bindingNavigatorPositionItem.Size = new System.Drawing.Size(50, 23);
            this.bindingNavigatorPositionItem.Text = "0";
            this.bindingNavigatorPositionItem.ToolTipText = "Current position";
            // 
            // bindingNavigatorCountItem
            // 
            this.bindingNavigatorCountItem.Name = "bindingNavigatorCountItem";
            this.bindingNavigatorCountItem.Size = new System.Drawing.Size(35, 15);
            this.bindingNavigatorCountItem.Text = "of {0}";
            this.bindingNavigatorCountItem.ToolTipText = "Total number of items";
            // 
            // bindingNavigatorSeparator1
            // 
            this.bindingNavigatorSeparator1.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator1.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorMoveNextItem
            // 
            this.bindingNavigatorMoveNextItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveNextItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveNextItem.Image")));
            this.bindingNavigatorMoveNextItem.Name = "bindingNavigatorMoveNextItem";
            this.bindingNavigatorMoveNextItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveNextItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveNextItem.Text = "Move next";
            // 
            // bindingNavigatorMoveLastItem
            // 
            this.bindingNavigatorMoveLastItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorMoveLastItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorMoveLastItem.Image")));
            this.bindingNavigatorMoveLastItem.Name = "bindingNavigatorMoveLastItem";
            this.bindingNavigatorMoveLastItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorMoveLastItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorMoveLastItem.Text = "Move last";
            // 
            // bindingNavigatorSeparator2
            // 
            this.bindingNavigatorSeparator2.Name = "bindingNavigatorSeparator";
            this.bindingNavigatorSeparator2.Size = new System.Drawing.Size(6, 6);
            // 
            // bindingNavigatorAddNewItem
            // 
            this.bindingNavigatorAddNewItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorAddNewItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorAddNewItem.Image")));
            this.bindingNavigatorAddNewItem.Name = "bindingNavigatorAddNewItem";
            this.bindingNavigatorAddNewItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorAddNewItem.Size = new System.Drawing.Size(23, 22);
            this.bindingNavigatorAddNewItem.Text = "Add new";
            // 
            // bindingNavigatorDeleteItem
            // 
            this.bindingNavigatorDeleteItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.bindingNavigatorDeleteItem.Image = ((System.Drawing.Image)(resources.GetObject("bindingNavigatorDeleteItem.Image")));
            this.bindingNavigatorDeleteItem.Name = "bindingNavigatorDeleteItem";
            this.bindingNavigatorDeleteItem.RightToLeftAutoMirrorImage = true;
            this.bindingNavigatorDeleteItem.Size = new System.Drawing.Size(23, 20);
            this.bindingNavigatorDeleteItem.Text = "Delete";
            // 
            // turmaBindingNavigatorSaveItem
            // 
            this.turmaBindingNavigatorSaveItem.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.turmaBindingNavigatorSaveItem.Image = ((System.Drawing.Image)(resources.GetObject("turmaBindingNavigatorSaveItem.Image")));
            this.turmaBindingNavigatorSaveItem.Name = "turmaBindingNavigatorSaveItem";
            this.turmaBindingNavigatorSaveItem.Size = new System.Drawing.Size(23, 23);
            this.turmaBindingNavigatorSaveItem.Text = "Save Data";
            this.turmaBindingNavigatorSaveItem.Click += new System.EventHandler(this.turmaBindingNavigatorSaveItem_Click);
            // 
            // nTurmaLabel
            // 
            nTurmaLabel.AutoSize = true;
            nTurmaLabel.Location = new System.Drawing.Point(14, 55);
            nTurmaLabel.Name = "nTurmaLabel";
            nTurmaLabel.Size = new System.Drawing.Size(49, 13);
            nTurmaLabel.TabIndex = 12;
            nTurmaLabel.Text = "n Turma:";
            // 
            // nTurmaTextBox
            // 
            this.nTurmaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "nTurma", true));
            this.nTurmaTextBox.Location = new System.Drawing.Point(69, 52);
            this.nTurmaTextBox.Name = "nTurmaTextBox";
            this.nTurmaTextBox.Size = new System.Drawing.Size(55, 20);
            this.nTurmaTextBox.TabIndex = 13;
            // 
            // cicloLetivonCicloLabel
            // 
            cicloLetivonCicloLabel.AutoSize = true;
            cicloLetivonCicloLabel.Location = new System.Drawing.Point(138, 55);
            cicloLetivonCicloLabel.Name = "cicloLetivonCicloLabel";
            cicloLetivonCicloLabel.Size = new System.Drawing.Size(97, 13);
            cicloLetivonCicloLabel.TabIndex = 13;
            cicloLetivonCicloLabel.Text = "Ciclo Letivon Ciclo:";
            // 
            // cicloLetivonCicloTextBox
            // 
            this.cicloLetivonCicloTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "CicloLetivonCiclo", true));
            this.cicloLetivonCicloTextBox.Location = new System.Drawing.Point(241, 52);
            this.cicloLetivonCicloTextBox.Name = "cicloLetivonCicloTextBox";
            this.cicloLetivonCicloTextBox.Size = new System.Drawing.Size(57, 20);
            this.cicloLetivonCicloTextBox.TabIndex = 14;
            // 
            // anoLetivoLabel
            // 
            anoLetivoLabel.AutoSize = true;
            anoLetivoLabel.Location = new System.Drawing.Point(311, 55);
            anoLetivoLabel.Name = "anoLetivoLabel";
            anoLetivoLabel.Size = new System.Drawing.Size(61, 13);
            anoLetivoLabel.TabIndex = 14;
            anoLetivoLabel.Text = "Ano Letivo:";
            // 
            // anoLetivoTextBox
            // 
            this.anoLetivoTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "AnoLetivo", true));
            this.anoLetivoTextBox.Location = new System.Drawing.Point(378, 52);
            this.anoLetivoTextBox.Name = "anoLetivoTextBox";
            this.anoLetivoTextBox.Size = new System.Drawing.Size(61, 20);
            this.anoLetivoTextBox.TabIndex = 15;
            // 
            // descriLabel
            // 
            descriLabel.AutoSize = true;
            descriLabel.Location = new System.Drawing.Point(453, 55);
            descriLabel.Name = "descriLabel";
            descriLabel.Size = new System.Drawing.Size(40, 13);
            descriLabel.TabIndex = 15;
            descriLabel.Text = "Descri:";
            // 
            // descriTextBox
            // 
            this.descriTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.turmaBindingSource, "Descri", true));
            this.descriTextBox.Location = new System.Drawing.Point(499, 52);
            this.descriTextBox.Name = "descriTextBox";
            this.descriTextBox.Size = new System.Drawing.Size(145, 20);
            this.descriTextBox.TabIndex = 16;
            // 
            // aLuno_TurmaBindingSource
            // 
            this.aLuno_TurmaBindingSource.DataMember = "AlunoTemTurma2";
            this.aLuno_TurmaBindingSource.DataSource = this.turmaBindingSource;
            // 
            // aLuno_TurmaTableAdapter
            // 
            this.aLuno_TurmaTableAdapter.ClearBeforeFill = true;
            // 
            // turmaDataGridView
            // 
            this.turmaDataGridView.AutoGenerateColumns = false;
            this.turmaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.turmaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.turmaDataGridView.DataSource = this.turmaBindingSource;
            this.turmaDataGridView.Location = new System.Drawing.Point(12, 96);
            this.turmaDataGridView.Name = "turmaDataGridView";
            this.turmaDataGridView.Size = new System.Drawing.Size(632, 170);
            this.turmaDataGridView.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "nTurma";
            this.dataGridViewTextBoxColumn4.HeaderText = "nTurma";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "CicloLetivonCiclo";
            this.dataGridViewTextBoxColumn5.HeaderText = "CicloLetivonCiclo";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "Descri";
            this.dataGridViewTextBoxColumn6.HeaderText = "Descri";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "AnoLetivo";
            this.dataGridViewTextBoxColumn7.HeaderText = "AnoLetivo";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            // 
            // aLuno_TurmaDataGridView
            // 
            this.aLuno_TurmaDataGridView.AutoGenerateColumns = false;
            this.aLuno_TurmaDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.aLuno_TurmaDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3});
            this.aLuno_TurmaDataGridView.DataSource = this.aLuno_TurmaBindingSource;
            this.aLuno_TurmaDataGridView.Location = new System.Drawing.Point(12, 289);
            this.aLuno_TurmaDataGridView.Name = "aLuno_TurmaDataGridView";
            this.aLuno_TurmaDataGridView.Size = new System.Drawing.Size(346, 80);
            this.aLuno_TurmaDataGridView.TabIndex = 16;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "ALunonProc";
            this.dataGridViewTextBoxColumn1.HeaderText = "ALunonProc";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "TurmanTurma";
            this.dataGridViewTextBoxColumn2.HeaderText = "TurmanTurma";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "dataMatricula";
            this.dataGridViewTextBoxColumn3.HeaderText = "dataMatricula";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // aLunonProcLabel
            // 
            aLunonProcLabel.AutoSize = true;
            aLunonProcLabel.Location = new System.Drawing.Point(369, 292);
            aLunonProcLabel.Name = "aLunonProcLabel";
            aLunonProcLabel.Size = new System.Drawing.Size(72, 13);
            aLunonProcLabel.TabIndex = 16;
            aLunonProcLabel.Text = "ALunon Proc:";
            // 
            // aLunonProcTextBox
            // 
            this.aLunonProcTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aLuno_TurmaBindingSource, "ALunonProc", true));
            this.aLunonProcTextBox.Location = new System.Drawing.Point(447, 289);
            this.aLunonProcTextBox.Name = "aLunonProcTextBox";
            this.aLunonProcTextBox.Size = new System.Drawing.Size(100, 20);
            this.aLunonProcTextBox.TabIndex = 17;
            // 
            // turmanTurmaLabel
            // 
            turmanTurmaLabel.AutoSize = true;
            turmanTurmaLabel.Location = new System.Drawing.Point(362, 322);
            turmanTurmaLabel.Name = "turmanTurmaLabel";
            turmanTurmaLabel.Size = new System.Drawing.Size(79, 13);
            turmanTurmaLabel.TabIndex = 17;
            turmanTurmaLabel.Text = "Turman Turma:";
            // 
            // turmanTurmaTextBox
            // 
            this.turmanTurmaTextBox.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.aLuno_TurmaBindingSource, "TurmanTurma", true));
            this.turmanTurmaTextBox.Location = new System.Drawing.Point(447, 319);
            this.turmanTurmaTextBox.Name = "turmanTurmaTextBox";
            this.turmanTurmaTextBox.Size = new System.Drawing.Size(100, 20);
            this.turmanTurmaTextBox.TabIndex = 18;
            // 
            // dataMatriculaLabel
            // 
            dataMatriculaLabel.AutoSize = true;
            dataMatriculaLabel.Location = new System.Drawing.Point(364, 353);
            dataMatriculaLabel.Name = "dataMatriculaLabel";
            dataMatriculaLabel.Size = new System.Drawing.Size(77, 13);
            dataMatriculaLabel.TabIndex = 18;
            dataMatriculaLabel.Text = "data Matricula:";
            // 
            // dataMatriculaDateTimePicker
            // 
            this.dataMatriculaDateTimePicker.DataBindings.Add(new System.Windows.Forms.Binding("Value", this.aLuno_TurmaBindingSource, "dataMatricula", true));
            this.dataMatriculaDateTimePicker.Location = new System.Drawing.Point(447, 349);
            this.dataMatriculaDateTimePicker.Name = "dataMatriculaDateTimePicker";
            this.dataMatriculaDateTimePicker.Size = new System.Drawing.Size(200, 20);
            this.dataMatriculaDateTimePicker.TabIndex = 19;
            // 
            // FormDataSetTurma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(658, 401);
            this.Controls.Add(dataMatriculaLabel);
            this.Controls.Add(this.dataMatriculaDateTimePicker);
            this.Controls.Add(turmanTurmaLabel);
            this.Controls.Add(this.turmanTurmaTextBox);
            this.Controls.Add(aLunonProcLabel);
            this.Controls.Add(this.aLunonProcTextBox);
            this.Controls.Add(this.aLuno_TurmaDataGridView);
            this.Controls.Add(this.turmaDataGridView);
            this.Controls.Add(descriLabel);
            this.Controls.Add(this.descriTextBox);
            this.Controls.Add(anoLetivoLabel);
            this.Controls.Add(this.anoLetivoTextBox);
            this.Controls.Add(cicloLetivonCicloLabel);
            this.Controls.Add(this.cicloLetivonCicloTextBox);
            this.Controls.Add(nTurmaLabel);
            this.Controls.Add(this.nTurmaTextBox);
            this.Controls.Add(this.turmaBindingNavigator);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDataSetTurma";
            this.Text = "FormDataSetTurma";
            this.Load += new System.EventHandler(this.FormDataSetTurma_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bDlocalDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaBindingNavigator)).EndInit();
            this.turmaBindingNavigator.ResumeLayout(false);
            this.turmaBindingNavigator.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aLuno_TurmaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.turmaDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.aLuno_TurmaDataGridView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button button2;
        private BDlocalDataSet bDlocalDataSet;
        private System.Windows.Forms.BindingSource turmaBindingSource;
        private BDlocalDataSetTableAdapters.TurmaTableAdapter turmaTableAdapter;
        private BDlocalDataSetTableAdapters.TableAdapterManager tableAdapterManager;
        private System.Windows.Forms.BindingNavigator turmaBindingNavigator;
        private System.Windows.Forms.ToolStripButton bindingNavigatorAddNewItem;
        private System.Windows.Forms.ToolStripLabel bindingNavigatorCountItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorDeleteItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveFirstItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMovePreviousItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator;
        private System.Windows.Forms.ToolStripTextBox bindingNavigatorPositionItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator1;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveNextItem;
        private System.Windows.Forms.ToolStripButton bindingNavigatorMoveLastItem;
        private System.Windows.Forms.ToolStripSeparator bindingNavigatorSeparator2;
        private System.Windows.Forms.ToolStripButton turmaBindingNavigatorSaveItem;
        private BDlocalDataSetTableAdapters.ALuno_TurmaTableAdapter aLuno_TurmaTableAdapter;
        private System.Windows.Forms.TextBox nTurmaTextBox;
        private System.Windows.Forms.TextBox cicloLetivonCicloTextBox;
        private System.Windows.Forms.TextBox anoLetivoTextBox;
        private System.Windows.Forms.TextBox descriTextBox;
        private System.Windows.Forms.BindingSource aLuno_TurmaBindingSource;
        private System.Windows.Forms.DataGridView turmaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridView aLuno_TurmaDataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.TextBox aLunonProcTextBox;
        private System.Windows.Forms.TextBox turmanTurmaTextBox;
        private System.Windows.Forms.DateTimePicker dataMatriculaDateTimePicker;
    }
}