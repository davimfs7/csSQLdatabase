﻿namespace csSQLdatabase
{
    partial class Jayce
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.catalinStalker = new System.Windows.Forms.Panel();
            this.cicloDeVida = new System.Windows.Forms.Timer(this.components);
            this.rubenStalker = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // catalinStalker
            // 
            this.catalinStalker.BackColor = System.Drawing.Color.DarkRed;
            this.catalinStalker.BackgroundImage = global::csSQLdatabase.Properties.Resources.catralin;
            this.catalinStalker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.catalinStalker.Location = new System.Drawing.Point(0, 0);
            this.catalinStalker.Name = "catalinStalker";
            this.catalinStalker.Size = new System.Drawing.Size(50, 50);
            this.catalinStalker.TabIndex = 0;
            this.catalinStalker.MouseEnter += new System.EventHandler(this.catalinStalker_MouseEnter);
            this.catalinStalker.MouseHover += new System.EventHandler(this.catalinStalker_MouseHover);
            // 
            // cicloDeVida
            // 
            this.cicloDeVida.Interval = 15;
            this.cicloDeVida.Tick += new System.EventHandler(this.cicloDeVida_Tick);
            // 
            // rubenStalker
            // 
            this.rubenStalker.BackColor = System.Drawing.Color.DodgerBlue;
            this.rubenStalker.BackgroundImage = global::csSQLdatabase.Properties.Resources.ruban;
            this.rubenStalker.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.rubenStalker.Location = new System.Drawing.Point(83, 43);
            this.rubenStalker.Name = "rubenStalker";
            this.rubenStalker.Size = new System.Drawing.Size(120, 120);
            this.rubenStalker.TabIndex = 1;
            // 
            // Jayce
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImage = global::csSQLdatabase.Properties.Resources.jayce;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(117, 79);
            this.Controls.Add(this.rubenStalker);
            this.Controls.Add(this.catalinStalker);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Jayce";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Jayce_Load);
            this.Click += new System.EventHandler(this.Jayce_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel catalinStalker;
        private System.Windows.Forms.Timer cicloDeVida;
        private System.Windows.Forms.Panel rubenStalker;
    }
}