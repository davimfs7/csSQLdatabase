﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace csSQLdatabase
{
    public partial class FormTurma : Form
    {
        Form main;
        private String BDsgbd,//vai permitir selecionar a base de dados para receber as 3 strings 
                       BDdml, //para atualizar a var e permitir ao OK executar a SQL DML
                       BDcodigo;//do alunos, caso se trate de um update ou insert

        MySqlConnection mySqlConn = null;
        SQLiteConnection sqliteConn = null;

        /*
         * Construtor publico de 4 argumentos, 3 strings e 1 Form para a referencia
         * Origem:
         * -Menu vem um pedido dmlSelect = "Inserir" para um determinado sgbd para um codigo selecionado
         * -FormList vem um sgbd = "Update" ou "Delete" para um sgbd, para um codigo selecionado na DataGrid
         */
        public FormTurma(Form form)
        {
            InitializeComponent();
            main = form;
        }

        public FormTurma(Form form, String sgbd, String dml, String codigo)
        {
            InitializeComponent();
            main = form;

            BDsgbd = sgbd;
            BDdml = dml;
            BDcodigo = codigo;

            
        }

        private void FormTurma_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bDlocalDataSet.CicloLetivo' table. You can move, or remove it, as needed.
            this.cicloLetivoTableAdapter.Fill(this.bDlocalDataSet.CicloLetivo);

            // TODO: This line of code loads data into the 'bDlocalDataSet.Turma' table. You can move, or remove it, as needed.
            this.turmaTableAdapter.Fill(this.bDlocalDataSet.Turma);

            //Adiciona o nome da BD e do comnado sql a usar no botão OK, ao titulo da form
            this.Text = BDsgbd + " " + BDdml;


            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(BDsgbd);     //Recebe a UtilsSQl uma ligação ao sgbd
            try
            {
                if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES turma");
                
                SqlCommand sqlCommand = new SqlCommand("Select Descr from CicloLetivo", sqlConn);    //Comando SQL DML
                //sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo); 
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();                                             //abertura a base de dados
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                Console.WriteLine(sqlDataReader.FieldCount);
                if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                {
                    while (sqlDataReader.Read())                             //Enquanto houver rows
                    {
                        Console.WriteLine(sqlDataReader["Descr"].ToString());
                        comboBoxDropBox.Items.Add(sqlDataReader["Descr"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Source.ToString() + "\n" + ex.TargetSite.ToString() + "\n" + ex.Message, "ERRO: FormAluno");

            }
            finally
            {
                sqlConn.Close();
            }

            //MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);

            //Alterar o nome do botão ok, de acordo com o dml
            btnOK.Text = BDdml;

            //se Insert, query à BD para obter o ultimo codigo pk

            //Se Update, usar o código passado no construtor para recolher os dados da BD

            //Se delete, usar o codigo passado no construtor para recolher os dados da BD
            //Definir os campos disable

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void turmaBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.turmaBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.bDlocalDataSet);

        }

        

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Hide();
            main.Show();
        }

        private void FormTurma_Shown(object sender, EventArgs e)
        {
            MessageBox.Show(BDsgbd + " | " + BDdml + " | " + BDcodigo);
        }


        private String getLastTablePk()
        {
            int pkCode = -1;                                                      // Recebe o codigo da tabela a usar nas SQL DML
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(BDsgbd);     //Recebe a UtilsSQl uma ligação ao sgbd
            try
            {
                if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES");

                SqlCommand sqlCommand = new SqlCommand("Select MAX(nProc) as nProc from Aluno", sqlConn);   //Comando SQL DML
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();                                             //abertura a base de dados
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                if (sqlDataReader.HasRows)                                   //Se houver rows lidas BD
                {
                    while (sqlDataReader.Read())                             //Enquanto houver rows
                    {
                        String temp = sqlDataReader["nProc"].ToString();    //Extrai o valor do codigo pk da tabela e converte para string
                        if (UtilsSQL.getTest()) MessageBox.Show(" Max pkCode na tabelas = " + temp, "TESTES; FormAluno");
                        if (temp == "")
                        {
                            MessageBox.Show(" Tabela Vazia. Ok para inserir o 1º regito", "INFO");
                            pkCode = 1;                                     //registo na textbox da codigo par a a

                        }
                        else
                        {
                            if (UtilsSQL.getTest()) MessageBox.Show(" Vou converter " + temp + " para INT.", "TESTES; FormAluno");
                            pkCode = int.Parse(temp) + 1;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Tabela Vazia 2. OK para inserir o 1º registo ", "INFO");
                    pkCode = 1;
                }


            }
            catch (Exception e)
            {
                MessageBox.Show(e.Source.ToString() + "\n" + e.TargetSite.ToString() + "\n" + e.Message, "ERRO: FormAluno");

            }
            finally
            {
                sqlConn.Close();
            }
            return pkCode.ToString();

        }

        private void getFieldsData()
        {
            SqlConnection sqlConn = (SqlConnection)UtilsSQL.selectSgbd(BDsgbd);

            try
            {
                if (UtilsSQL.getTest()) MessageBox.Show(" DML = " + BDdml + "\nVou fazer a query", "TESTES: getFieldsData()");

                SqlCommand sqlCommand = new SqlCommand("Select NAluno, nome from Aluno where nProc = @codigo", sqlConn);    //Comando SQL DML
                sqlCommand.Parameters.AddWithValue("@codigo", BDcodigo);
                sqlCommand.CommandType = CommandType.Text;

                sqlConn.Open();                                             //abertura a base de dados
                SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();   //data reader lê registos (rows ) da BD
                if (sqlDataReader.HasRows)                                  //Se houver rows lidas BD
                {
                    while (sqlDataReader.Read())                            //Enquanto houver rows
                    {
                        //textBoxNome.Text = sqlDataReader["Nome"].ToString();
                        //textBoxNumAluno.Text = sqlDataReader["NAluno"].ToString();
                    }
                }
                else MessageBox.Show(" Não foram encontrados dados para o registo de código: " + BDcodigo, "INFO");

            }
            catch (Exception e)
            {
                MessageBox.Show("Erro BD:\n" + e.Message, "FormAluno - getFieldsData()");
            }
            finally
            {
                sqlConn.Close();
            }
        }

        private void descriLabel1_Click(object sender, EventArgs e)
        {

        }

        private void descriTextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void turmaComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void nCicloLabel_Click(object sender, EventArgs e)
        {

        }
    }
}
