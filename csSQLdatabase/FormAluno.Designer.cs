﻿namespace csSQLdatabase
{
    partial class FormAluno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.textBoxNome = new System.Windows.Forms.TextBox();
            this.labelNome = new System.Windows.Forms.Label();
            this.textBoxNumAluno = new System.Windows.Forms.TextBox();
            this.labelNumALuno = new System.Windows.Forms.Label();
            this.textBoxNProc = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.BackColor = System.Drawing.Color.LimeGreen;
            this.btnOK.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnOK.Location = new System.Drawing.Point(456, 227);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 23;
            this.btnOK.Text = "OK";
            this.btnOK.UseVisualStyleBackColor = false;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.BackColor = System.Drawing.Color.IndianRed;
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCancelar.Location = new System.Drawing.Point(537, 227);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 22;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = false;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // textBoxNome
            // 
            this.textBoxNome.Location = new System.Drawing.Point(55, 65);
            this.textBoxNome.Name = "textBoxNome";
            this.textBoxNome.Size = new System.Drawing.Size(206, 20);
            this.textBoxNome.TabIndex = 21;
            // 
            // labelNome
            // 
            this.labelNome.AutoSize = true;
            this.labelNome.Location = new System.Drawing.Point(13, 65);
            this.labelNome.Name = "labelNome";
            this.labelNome.Size = new System.Drawing.Size(35, 13);
            this.labelNome.TabIndex = 20;
            this.labelNome.Text = "Nome";
            // 
            // textBoxNumAluno
            // 
            this.textBoxNumAluno.Location = new System.Drawing.Point(55, 39);
            this.textBoxNumAluno.Name = "textBoxNumAluno";
            this.textBoxNumAluno.Size = new System.Drawing.Size(36, 20);
            this.textBoxNumAluno.TabIndex = 19;
            // 
            // labelNumALuno
            // 
            this.labelNumALuno.AutoSize = true;
            this.labelNumALuno.Location = new System.Drawing.Point(13, 39);
            this.labelNumALuno.Name = "labelNumALuno";
            this.labelNumALuno.Size = new System.Drawing.Size(42, 13);
            this.labelNumALuno.TabIndex = 18;
            this.labelNumALuno.Text = "NAluno";
            // 
            // textBoxNProc
            // 
            this.textBoxNProc.Enabled = false;
            this.textBoxNProc.Location = new System.Drawing.Point(55, 13);
            this.textBoxNProc.Name = "textBoxNProc";
            this.textBoxNProc.Size = new System.Drawing.Size(36, 20);
            this.textBoxNProc.TabIndex = 17;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 16;
            this.label1.Text = "nProc";
            // 
            // FormAluno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(624, 262);
            this.Controls.Add(this.btnOK);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.textBoxNome);
            this.Controls.Add(this.labelNome);
            this.Controls.Add(this.textBoxNumAluno);
            this.Controls.Add(this.labelNumALuno);
            this.Controls.Add(this.textBoxNProc);
            this.Controls.Add(this.label1);
            this.Name = "FormAluno";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormAluno";
            this.Load += new System.EventHandler(this.FormAluno_Load);
            this.Shown += new System.EventHandler(this.FormAluno_Shown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.TextBox textBoxNome;
        private System.Windows.Forms.Label labelNome;
        private System.Windows.Forms.TextBox textBoxNumAluno;
        private System.Windows.Forms.Label labelNumALuno;
        private System.Windows.Forms.TextBox textBoxNProc;
        private System.Windows.Forms.Label label1;
    }
}